package com.molyfun.dao.sys;

import java.util.List;

import com.molyfun.core.base.BaseExpandMapper;

public interface SysSessionExpandMapper extends BaseExpandMapper {

	void deleteBySessionId(String sessionId);

	Integer queryBySessionId(String sessionId);

	List<String> querySessionIdByAccount(String account);

}
