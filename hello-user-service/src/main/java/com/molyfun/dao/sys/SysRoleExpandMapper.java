package com.molyfun.dao.sys;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.molyfun.core.base.BaseExpandMapper;

public interface SysRoleExpandMapper extends BaseExpandMapper {

	List<String> queryPermission(@Param("roleId") Integer id);

}
