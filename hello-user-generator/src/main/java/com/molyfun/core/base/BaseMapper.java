package com.molyfun.core.base;

import java.util.List;

/**
 */
public interface BaseMapper<T extends BaseModel> {
	List<T> selectAll();

	int deleteByPrimaryKey(String id);

	T selectByPrimaryKey(String id);

	int insert(T record);

	int updateByPrimaryKey(T record);
}
