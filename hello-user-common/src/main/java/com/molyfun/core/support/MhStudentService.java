package com.molyfun.core.support;

import com.molyfun.core.base.BaseModel;

public interface MhStudentService {
	
	public BaseModel login(String account,String password);
	
	public BaseModel queryByToken(String token);
	
//	public void updateToken(String token);

	/**
	 * @param id
	 * @return
	 */
	public BaseModel queryById(String id);
	
	
}
